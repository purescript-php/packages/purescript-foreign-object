# purescript-foreign-object

[![Latest release](http://img.shields.io/github/release/purescript/purescript-foreign-object.svg)](https://github.com/purescript/purescript-foreign-object/releases)
[![Build status](https://travis-ci.org/purescript/purescript-foreign-object.svg?branch=master)](https://travis-ci.org/purescript/purescript-foreign-object)

Functions for working with homogeneous JavaScript objects from PureScript.

## Installation

```
bower install purescript-foreign-object
```

## Documentation

Module documentation is [published on Pursuit](http://pursuit.purescript.org/packages/purescript-foreign-object).
