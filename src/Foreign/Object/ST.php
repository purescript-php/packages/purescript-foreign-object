<?php

$exports['new'] = function () {
  return [];
};

$exports['peekImpl'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($k) use (&$just, &$nothing) {
      return function ($m) use (&$just, &$nothing, &$k) {
        return function () use (&$just, &$nothing, &$k, &$m) {
          return array_key_exists($k, $m) ? $just($m[$k]) : $nothing;
        };
      };
    };
  };
};

$exports['poke'] = function ($k) {
  return function ($v) use (&$k) {
    return function ($m) use (&$v, &$k) {
      return function () use (&$m, &$v, &$k) {
        $m[$k] = $v;
        return $m;
      };
    };
  };
};

$exports['delete'] = function ($k) {
  return function ($m) use (&$k) {
    return function () use (&$k, &$m) {
      unset($m[$k]);
      return $m;
    };
  };
};
