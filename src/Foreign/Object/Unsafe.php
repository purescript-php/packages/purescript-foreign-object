<?php
$exports['unsafeIndex'] = function ($m) {
  return function ($k) use (&$m) {
    return $m[$k];
  };
};
